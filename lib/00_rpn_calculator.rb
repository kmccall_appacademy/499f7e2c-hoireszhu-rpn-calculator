class RPNCalculator

  def initialize
    @stack = []
  end

  def push(num)
    @stack << num
  end

  def value
    @stack.last
  end

  def plus
    raise "calculator is empty" if @stack.length < 2
    result = @stack.pop + @stack.pop
    @stack << result
  end

  def minus
    raise "calculator is empty" if @stack.length < 2
    result = -(@stack.pop - @stack.pop)
    @stack << result
  end

  def times
    raise "calculator is empty" if @stack.length < 2
    result = @stack.pop * @stack.pop
    @stack << result
  end

  def divide
    raise "calculator is empty" if @stack.length < 2
    result = 1.fdiv(@stack.pop.fdiv(@stack.pop))
    @stack << result
  end

  def tokens(str)
    buffer = str.split
    operations = [:+,:-,:*,:/]
    buffer.map{|char| operations.include?(char.to_sym) ? char.to_sym : char.to_i}
  end

  def evaluate(str)
    buffer = str.split
    operations = [:+,:-,:*,:/]
    buffer.each do |char|
      if operations.include?(char.to_sym)
        case char
        when "+"
          plus
        when "-"
          minus
        when "*"
          times
        when "/"
          divide
        end
      else
        push(char.to_i)
      end
    end

    value
  end

end
